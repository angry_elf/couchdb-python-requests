Client module
=============

class Server
------------
.. autoclass:: couchdbrq.client.Server
   :members:

class Database
--------------
.. autoclass:: couchdbrq.client.Database
   :members:


class Document
--------------
.. autoclass:: couchdbrq.client.Document
   :members:

class ViewResults
-----------------
.. autoclass:: couchdbrq.client.ViewResults
   :members:
